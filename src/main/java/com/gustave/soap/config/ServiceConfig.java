package com.gustave.soap.config;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.ws.wsdl.wsdl11.Wsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

@EnableWs
@Configuration
public class ServiceConfig {
    @Bean
    public ServletRegistrationBean messageDispatcherServlet(ApplicationContext context) {
        MessageDispatcherServlet messageDispatcherServlet = new MessageDispatcherServlet();
        messageDispatcherServlet.setApplicationContext(context);
        messageDispatcherServlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean(messageDispatcherServlet, "/ws/gustave/*");
    }


    @Bean(name = "suppliers")
    public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema supplierSchema) {
        DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
        definition.setPortTypeName("SupplierPort");
        definition.setTargetNamespace("http://gustave.com/supplier");
        definition.setLocationUri("/ws/gustave");
        definition.setSchema(supplierSchema);
        return definition;
    }

    @Bean
    public XsdSchema coursesSchema() {
        return new SimpleXsdSchema(new ClassPathResource("supplier.xsd"));
    }

//    @Bean(name = "items")
//    public Wsdl11Definition studentDefinition(XsdSchema itemsSchema) {
//        DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
//        definition.setPortTypeName("StudentPort");
//        definition.setTargetNamespace("http://gustavecom/items");
//        definition.setLocationUri("/ws/gustave");
//        definition.setSchema(itemsSchema);
//        return definition;
//    }
//
//    @Bean
//    public XsdSchema studentsSchema() {
//        return new SimpleXsdSchema(new ClassPathResource("items.xsd"));
//    }

}
