package com.gustave.soap.repository;

import com.gustave.soap.bean.Items;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IItemsRepository extends JpaRepository<Items,Long> {
}
