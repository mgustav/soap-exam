package com.gustave.soap.repository;

import com.gustave.soap.bean.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ISupplierRepository extends JpaRepository<Supplier,Long> {
}
