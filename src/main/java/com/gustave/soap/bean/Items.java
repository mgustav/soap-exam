package com.gustave.soap.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "items")
public class Items {
    @Id
    @GeneratedValue
    private long id;
    private String name;
    private long item_code;
    private double price;

}
