package com.gustave.soap.bean;

import javax.persistence.*;

@Entity
@Table(name = "supplier")
public class Supplier {
    @Id
    @GeneratedValue
    private long id;

    private String names;
    private long mobile;
    private String email;

    @OneToOne
    private Items items;

    public Supplier(){};

    public Supplier(String names, long mobile, String email) {
        this.names = names;
        this.mobile = mobile;
        this.email = email;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public long getMobile() {
        return mobile;
    }

    public void setMobile(long mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
